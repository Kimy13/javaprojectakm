# Calcul des instructions de navigation pour un itinéraire à pied


## Description
Ce programme, réalisé en JAVA par l'action conjointe de Marie Lê, Kim Monoury--Homet et Anna Roussel, génère des instructions écrites pour permettre à l'utilisateur de se repérer tout au long de son itinéraire. Les noms de rue n'étant pas toujours visibles en ville, les points de repère utilisés par le programme sont des commerces ou d'autres bâtiments reconnaissables comme des postes ou des bâtiments officiels.


## Installation
Il fonctionne de la façon suivante : 

1. Exécutez le code de la classe InterfaceGraphique se trouvant dans le package eu.ensg.portail, un fond blanc ainsi que plusieurs onglets apparaissent.
2. Cliquez sur l'onglet nommé "File" pour afficher le fond de carte. Si cela ne fonctionne pas, n'hésitez pas à cliquer plusieurs fois sur cet onglet. (Il faut juste laisser le temps à l'ordinateur d'exécuter certaines instructions).
3. Remplissez les cases où il est écrit Coordonnées de départ et Coordonnées d'arrivée (en effaçant en amont les écritures qu'elles contiennent) par la latitude et la longitude sous la forme suivante : latitude,longitude.
4. Cliquez sur le bouton "Valider".
5. Les instructions et le temps de trajet s'affichent.


## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Kimy13/javaprojectakm.git
git branch -M main
git push -uf origin main
```
